/**
 * Created by lichong on 2/22/2018.
 */

import React from 'react';
import {Row, Col, Radio , Divider} from 'antd';

const RadioGroup = Radio.Group;

class PayMethodSelector extends React.Component {

    state = {
        payCode : 'alipay'
    }


    constructor(props) {
        super(props)
        this.props.setPayMethod(this.state.payCode)
    }

    onChange(e){
        this.props.setPayMethod(e.target.value)
    }

    render() {
        return (
            <div>
                <Row>
                    <Col span={2}>
                        <h3>支付方式</h3>
                    </Col>
                </Row>
                <Row>
                    <RadioGroup defaultValue={this.state.payCode} onChange={(c) => this.onChange(c)}>
                        <Radio value={'alipay'}>支付宝</Radio>
                        <Radio value={'cash'}>货到付款</Radio>
                    </RadioGroup>
                </Row>
                <Divider/>
            </div>
        );
    }
}

export default PayMethodSelector;