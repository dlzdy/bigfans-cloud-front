import React, {Component} from 'react';
import {Icon, Menu} from 'antd';

import HttpUtils from '../../utils/HttpUtils.js';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

class CategoryNavigator extends Component {

    state = {
        categories: []
    }

    componentDidMount() {
        var self = this;
        HttpUtils.getNavigator({}, {
            success: function (response) {
                self.setState({categories: response.data});
            }
        });
    }


    render() {
        return (
            <div>
                <Menu style={{width: 256}} mode="vertical">
                    {
                        this.state.categories.map((cat, index) => {
                            return (
                                <SubMenu key={cat.id}
                                         title={<a href={'/search?catId=' + cat.id}><Icon type="setting"/><span>{cat.name}</span></a>}>
                                    {
                                        cat.subCats.map((subcat, ind) => {
                                            return (
                                                <Menu.Item key={subcat.id}>
                                                    <a href={'/search?catId=' + subcat.id}>
                                                        {subcat.name}
                                                    </a>
                                                </Menu.Item>
                                            )
                                        })
                                    }
                                </SubMenu>
                            );
                        })
                    }
                </Menu>
            </div>
        );
    }
}

export default CategoryNavigator;
